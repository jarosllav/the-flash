#pragma once
#pragma disable in ewah.h
#pragma warning (disable : 4146)

#include <string>
#include <fstream>
#include <vector>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/Rect.hpp>

#include "States/Play/Player.hpp"

struct Cell
{
	sf::Vector2f position;
	std::uint16_t id;
	bool isCollision = false;

	Cell() = default;
	Cell( std::uint16_t id_, sf::Vector2f position_, bool _isCollision ) :
		id(id_), position(position_), isCollision(_isCollision)
	{
	}

	inline bool IsCollider() { return this->isCollision; }
};

class Map
{
	const sf::Vector2u tileSize = { 64u, 64u };
public:
	Map() {}
	Map( const std::string& path )
	{
		this->LoadFromFile( path );
	}

	bool LoadFromFile( const std::string& path );
	void UpdateTilesVerticies();
	void Draw( sf::RenderTarget& target, sf::RenderStates states );

	bool IsCollision( Player* );
	//bool IsCollision( const DIRECTION& dir );

	Cell* GetCell( std::int16_t x, std::int16_t y );

	std::vector<Cell*> GetTiles();
	std::uint16_t getVersion();
	sf::Texture* getTileSet();

private:
	sf::Vector2u size;
	std::uint16_t version;
	sf::Texture tileSet;
	
	std::vector<Cell> tiles;
	std::vector<Cell> tilesCollision;

	sf::VertexArray vertices;
	unsigned int collPosition;
};