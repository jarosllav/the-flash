#pragma once

#include <string>

#include "App/State.hpp"
#include "Config.hpp"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/View.hpp>

#include <SFML/Graphics/RectangleShape.hpp>

struct Button
{
	sf::Texture texture;
	sf::Sprite sprite;

	Button( const std::string& data, const sf::Vector2f& position )
	{
		if ( !texture.loadFromFile( data ) ) return;

		sprite.setTexture( texture );
		sprite.setPosition( position );
	}

	bool IsClicked( const sf::Vector2f& position )
	{
		auto pos = sprite.getPosition();
		auto size = texture.getSize();
		size.x = size.x * sprite.getScale().x;
		size.y = size.y * sprite.getScale().y;
		if ( position.x >= pos.x && position.x <= pos.x + size.x && position.y >= pos.y && position.y <= pos.y + size.y )
			return true;
		return false;
	}

	void Draw( sf::RenderTarget& target, sf::RenderStates states )
	{
		target.draw( this->sprite, states );
	}
};

class MenuState final : public State
{
public:
	MenuState();

	short Run() override;
private:
	Button play;
	Button quit;

	//sf::View view;

};