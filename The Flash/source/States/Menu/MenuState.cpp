#include "MenuState.hpp"

MenuState::MenuState() :
	play("data/textures/ui/menu/btn_play.png", sf::Vector2f(100, 100)),
	quit("data/textures/ui/menu/btn_quit.png", sf::Vector2f(100, 300))
{
	play.sprite.setScale( { 0.3f, 0.3f } );
	quit.sprite.setScale( { 0.3f, 0.3f } );
}

short MenuState::Run()
{
	this->view->reset( sf::FloatRect( { 0.0, 0.0 }, sf::Vector2f( cfg::app::window.x, cfg::app::window.y ) ) );
	this->view->setCenter( cfg::app::window.x / 2, cfg::app::window.y / 2 );

	sf::RectangleShape shape;
	shape.setFillColor( sf::Color::Transparent );
	shape.setOutlineColor( sf::Color::Red );
	shape.setOutlineThickness( 1 );

	sf::Event ev;
	while ( window->isOpen() )
	{
		while ( window->pollEvent( ev ) )
		{
			if ( ev.type == sf::Event::Closed )
				return (short)-1;
			else if ( ev.type == sf::Event::MouseButtonPressed && sf::Mouse::isButtonPressed(sf::Mouse::Left) ){
				auto mouse = sf::Mouse::getPosition( *this->window );
				if ( play.IsClicked(sf::Vector2f(mouse.x, mouse.y) ) ) {
					return (short)1;
				} 
				else if ( quit.IsClicked(sf::Vector2f(mouse.x, mouse.y) ) ) {
					return (short)-1;
				}
			}

		}

		window->clear(sf::Color::Black);
		window->setView( *this->view );

		play.Draw( *this->window, sf::RenderStates::Default );
		quit.Draw( *this->window, sf::RenderStates::Default );

		auto mouse = sf::Mouse::getPosition( *this->window );
		if ( play.IsClicked( sf::Vector2f( mouse.x, mouse.y ) ) ) {
			auto pos = play.sprite.getPosition();
			auto size = play.texture.getSize();
			size.x = size.x * play.sprite.getScale().x;
			size.y = size.y * play.sprite.getScale().y;

			shape.setSize( sf::Vector2f( size.x, size.y ) );
			shape.setPosition( pos );
			window->draw( shape );
		}
		else if ( quit.IsClicked(sf::Vector2f(mouse.x, mouse.y) ) ) {
			auto pos = quit.sprite.getPosition();
			auto size = quit.texture.getSize();
			size.x = size.x * quit.sprite.getScale().x;
			size.y = size.y * quit.sprite.getScale().y;

			shape.setSize( sf::Vector2f( size.x, size.y ) );
			shape.setPosition( pos );
			window->draw( shape );		
		}

		window->display();
	}
	return (short)-1;
}