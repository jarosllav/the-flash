#include "Player.hpp"

Player::Player(const sf::Vector2f& size)
{
}

void Player::Create(sf::Texture& texture)
{
	this->moveAnim = Animation( &texture, sf::Vector2u( 3, 1 ), 0.3f );
	this->sprite.setTexture( texture );
}

void Player::Update( float dt )
{
	this->moveAnim.Update( 0, dt );
}

void Player::Move( const sf::Vector2f& position )
{
	this->pos.x += position.x;
	this->pos.y += position.y;

	this->sprite.setPosition( this->pos );
}

void Player::Draw( sf::RenderTarget& win )
{
	this->sprite.setTextureRect( this->moveAnim.uvRect );
	win.draw( this->sprite );
}

MOVE_DIR Player::GetDirection( const MOVE_AXIS& axis )
{
	if ( axis == MOVE_AXIS::X ) return this->dirX;
	else if ( axis == MOVE_AXIS::Y ) return this->dirY;

	return MOVE_DIR::NONE;
}

void Player::SetDirection( const MOVE_AXIS& axis, const MOVE_DIR& dir )
{
	if ( axis == MOVE_AXIS::X ) this->dirX = dir;
	else if ( axis == MOVE_AXIS::Y ) this->dirY = dir;
}