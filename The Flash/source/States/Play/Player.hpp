#pragma once

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Window/Keyboard.hpp>

#include "Animation.hpp"

enum MOVE_DIR
{
	NONE = 0,
	DOWN,
	UP,
	LEFT,
	RIGHT
};

enum MOVE_AXIS
{
	X = 0,
	Y
};

class Player
{
public:
	Player() = default;
	Player(const sf::Vector2f&);
	void Create(sf::Texture&);

	void Move( const sf::Vector2f& );
	void Draw(sf::RenderTarget&);
	void Update( float dt );

	inline void SetTexture( sf::Texture texture ) { this->sprite.setTexture( texture ); }
	inline sf::Sprite& GetSprite() { return this->sprite; }
	inline sf::Vector2f GetPosition() { return this->pos; }
	inline float GetAngle() { return this->angle; }
	inline unsigned int GetSpeed() { return this->speed; }
	inline float GetSprint() { return this->sprint; }
	inline void SetSprint( float val ) { this->sprint = val; }

	MOVE_DIR GetDirection( const MOVE_AXIS& axis );
	void SetDirection( const MOVE_AXIS& axis, const MOVE_DIR& dir );
private:
	sf::Vector2f pos;
	float angle;

	float sprint = 0.0;

	unsigned int speed = 5;
	MOVE_DIR dirX = MOVE_DIR::NONE;
	MOVE_DIR dirY = MOVE_DIR::NONE;

	sf::Sprite sprite;

	Animation moveAnim;
};