#include "Animation.hpp"

Animation::Animation( sf::Texture* texture, const sf::Vector2u& imgCount, float switchTime )
{
	this->imgCount = imgCount;
	this->switchTime = switchTime;
	totalTime = 0.0f;
	currImg.x = 0;

	uvRect.width = texture->getSize().x / float( imgCount.x );
	uvRect.height = texture->getSize().y / float( imgCount.y );
}

Animation::~Animation(){}

void Animation::Update( int row, float time )
{
	currImg.y = row;
	totalTime += time;

	if ( totalTime >= switchTime ) {
		totalTime -= switchTime;
		++currImg.x;
		
		if ( currImg.x >= imgCount.x ) {
			currImg.x = 0;
		}
	}

	uvRect.left = currImg.x * uvRect.width;
	uvRect.top = currImg.y * uvRect.height;
}