#pragma once

#include "Config.hpp"

#include "App/State.hpp"

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/View.hpp>

#include "States/Play/Player.hpp"

#include "Resource/ResourceHolder.hpp"

#include "Map/MapManager.hpp"

class PlayState final : public State
{
public:
	PlayState();

	short Run() override;

private:
	Player player;
	MapManager mapManager;
};