#include "App.hpp"

App::App() :
	stateMachine(0)
{
	this->window.create( sf::VideoMode( cfg::app::window.x, cfg::app::window.y, 32u ), cfg::app::title, sf::Style::Default );
	this->window.setFramerateLimit( 60u );
	this->view.reset( sf::FloatRect( { 0.0, 0.0 }, sf::Vector2f( cfg::app::window.x, cfg::app::window.y ) ) );
}

App::~App()
{

}

void App::Run()
{

	stateMachine.Run();

}