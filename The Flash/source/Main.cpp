#include <iostream>
#include <string>

#include "App/App.hpp"

#include "States/Menu/MenuState.hpp"
#include "States/Play/PlayState.hpp"

int main()
{
	std::cout << "The Flash is comming!\n";

	App app;

	app.AddState<MenuState>( (short)0 );
	app.AddState<PlayState>( (short)1 );

	app.Run();

	return 0;
}